"""File is responsible for generating .md files for each city,
later these .md files will be used to generate html pages.
"""

import os
from datetime import date


def about_city(city_name):

    about = f"""{city_name} is a Himalayan resort town in the Kumaon region of India’s Uttarakhand state,
    at an elevation of roughly 2,000m. Formerly a British hill station, it’s set around {city_name} Lake,
    a popular boating site with Naina Devi Hindu Temple on its north shore.
    A cable car runs to Snow View observation point (at 2,270m),
    with vistas over the town and mountains including Nanda Devi, Uttarakhand’s highest peak"""

    return about


def generate_md_file():

    data_path = os.getenv("DATA_PATH")

    file_name = os.listdir(data_path)
    for file in file_name:

        # file is csv file for each city
        # to get city name we have to split the file name.
        title = file.split('.')[0]

        # current date
        current_date = date.today()

        about = about_city(title)

        md_file_format(title, current_date, title, title+' about', title, about)

    return


def md_file_format(title, date, tags, summary, slug, about):
    text = f"""---
title: {title}
date: {date}
tags: {tags}
thumbnail: img/challah.jpg
summary: {summary}
slug: {slug}
---

__About__

{about}
"""

    with open(f"content/{slug}.md", "w") as f:
        f.write(text)

    return text.strip()


if __name__ == "__main__":
    generate_md_file()
# generate_md("{city_name}", "2019-04-10", "Delhi", "{city_name} about", "{city_name}", "{city_name} is a Himalayan resort town in the Kumaon region of India’s Uttarakhand state, at an elevation of roughly 2,000m. Formerly a British hill station, it’s set around {city_name} Lake, a popular boating site with Naina Devi Hindu Temple on its north shore. A cable car runs to Snow View observation point (at 2,270m), with vistas over the town and mountains including Nanda Devi, Uttarakhand’s highest peak")
