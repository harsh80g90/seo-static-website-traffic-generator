# SEO static Website Traffic Generator

Seo static Website Traffic Generator used to fetch the data from MySql and visualize the data on the Web.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

```bash
python3 -m venv ENV
```

```bash
pip3 install -r requirements.txt
```

```bash
./run.sh 
```

### Running the Service

* Run db.py for making connection with MySql server and saving the data
* run html_generator.py (for generating
  static web page (header + chart + footer)

## Built With

* Python, JavaScript, HTML, CSS

```
more details will be added soon.
 
```
