"""File is responsible for fetching the data from MySql server.

and save it into data frame so that we can use it later to build our
data visualization system on web.
"""


import os
from sqlalchemy import create_engine


def get_connection():
    """Get database credentials and stablish database connection.

    Returns:-
    db: Raw database connection.
    """
    # get environment variable
    user = os.getenv("DBUSER")
    passwd = os.getenv("DBPASS")
    host = os.getenv("DBHOST")
    database = os.getenv("DBNAME")
    dbport = os.getenv("DBPORT")
    # connection string
    con_string = "mysql+pymysql://{}:{}@{}:{}/{}".format(
        user, passwd, host, dbport, database)

    # create engine
    sqlEngine = create_engine(con_string, pool_recycle=3600)
    db = sqlEngine.raw_connection()
    return db


# get_data function in responsible for fetching the data and saving into the
def get_data(db, query):
    """
    Get data from database for a specific query.

    Parameters:-
    db: Database connection
    query(string): Search query

    Returns:-
    data: data from database
    """
    db = get_connection()
    cur = db.cursor()

    data = []
    cur.execute(query)

    job_data = cur.fetchall()

    for row in job_data:
        data.append(row)

    cur.close()
    db.close()

    return data


def update_urls(city, url):
    """Update_urls function insert city names and city urls into database.

    Parameters:-
    city: city name
    url: city url
    """
    db = get_connection()
    cur = db.cursor()

    query = "INSERT INTO url_data (url, city) VALUES (%s, %s)"
    values = (url, city)

    cur.execute(query, values)

    db.commit()
    cur.close()
    db.close()
