# This Static class is used for generating,
# HEADER, FOOTER and CHART section.

class Static:

    def header(self):
        header_text = """
                        <!DOCTYPE html>
                        <html lang="en">
                            <head>
                            <link  type="text/css" href="css/style.css">
                            <meta charset="utf-8" />
                            <!-- CSS only -->
                            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
                            </head>
                        <body>
                    """

        return header_text

    def chart(self):
        chart_section = """
                        <div id="wrapper">
                            <canvas id="canvas">
                                <!-- your chart will be generated here! -->
                            </canvas>
                        </div>
                        <hr>
                        <div id="wrapper">
                            <canvas id="myChart">
                                <!-- your chart will be generated here! -->
                            </canvas>
                        </div>

                        <hr>

                        <center>
                            <div id="tableSection"></div>
                        </center>
            """
        return chart_section

    def footer(self, city="static"):
        data_layer = "{dataLayer.push(arguments)};"
        footer_text = f"""
                        </body>
                            <!-- Global site tag (gtag.js) - Google Analytics -->
                            <script
                                async
                                src="https://www.googletagmanager.com/gtag/js?id=UA-167972737-1"
                            ></script>
                            <script>
                                window.dataLayer = window.dataLayer || [];
                                function gtag() {
                                data_layer
                                }
                                gtag("js", new Date());

                                gtag("config", "UA-167972737-1");
                            </script>
                            <!-- loads chart.js, d3, and your visualization -->
                            <!-- JavaScript Bundle with Popper -->
                            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
                            <script src="https://d3js.org/d3.v6.min.js"></script>
                            <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
                            <script src="js/{city}.js"></script>
                        </html>

             """
        return footer_text
