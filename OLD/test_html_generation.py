import unittest
from html_generator import generate_html


class TestHtmlGenerator(unittest.TestCase):

    def test_generate_html(self):
        self.assertEqual(generate_html(
            "*this is some", "*this is some 2", "test"
        ), "https://xjobz.com/open-positions/report/test.html")


if __name__ == "__main__":
    unittest.main()
