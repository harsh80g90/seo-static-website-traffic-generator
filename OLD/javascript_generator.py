"""File is responsible for generating javascript files and bar charts.

Files generated with this process will be saved in data folder.
"""

import os


def generate_javascript(name):
    """
    Generate javascript graph from city name.

    Parameters:-
    name(string): city name.
    """
    java_script = """
                // csv file we want to load
                let filename = "data/csv_data/""" + name + """.csv";
                let title = "Average Income and Population In City";

                // all of your code should be inside this command
                d3.csv(filename).then(function (loadedData) {
                // let's see if our data loaded correctly
                // (and take a peek at how it's formatted)

                // console.log(loadedData);

                // empty lists for our data and the labels
                let data = [];
                let data2 = [];
                let labels = [];

                // use a for-loop to load the data from the
                // file into our lists
                for (let i = 0; i < loadedData.length; i++) {
                    // console.log(loadedData[i]);

                    // get the year and mean temp for each listing
                    // note: the 'keys' here correspond to the headers
                    // in our file and need to be typed exactly
                    let city_name = loadedData[i].job_category;
                    let average_income = loadedData[i].salary;
                    let population = loadedData[i].city;
                    // console.log(average_income);

                    // add the city to our labels
                    labels.push(city_name);

                    // and mean average_income to the data
                    data.push(average_income);
                    data2.push(population);
                }

                // basic line chart settings
                let options = {
                    type: "bar",
                    data: {
                    labels: labels, // the labels we loaded!
                    datasets: [
                        {
                        data: data, // the data we loaded!
                        fill: false,
                        pointRadius: 5,
                        pointHoverRadius: 10,
                        borderColor: "rgb(255,150,0)",
                        backgroundColor: "rgb(255,150,0)",
                        label: "Average Income",
                        },
                        {
                        data: data2,
                        borderColor: "rgb(255,150,0)",
                        backgroundColor: "rgb(255,0,0)",
                        label: "Population",
                        },
                    ],
                    },
                    options: {
                    title: {
                        display: true,
                        text: title,
                    },
                    legend: {
                        display: true,
                    },
                    scales: {
                        yAxes: [
                        {
                            // yAxes label
                            scaleLabel: {
                            display: true,
                            labelString: "Average Income/Population",
                            },
                        },
                        ],
                        xAxes: [
                        {
                            // yAxes label
                            scaleLabel: {
                            display: true,
                            labelString: "City Name",
                            },
                        },
                        ],
                    },
                    },
                };

                Chart.defaults.global.defaultFontSize = 20;

                // with all that done, we can create our chart!
                let chart = new Chart(document.getElementById("myChart"),
                                                                options);
                });
                // data table start
                d3.text(filename).then(function (data) {
                var rows = d3.csvParseRows(data),
                table = d3
                    .select("#tableSection")
                    .append("table")
                    .style("border-collapse", "collapse")
                    .style("border", "2px black solid");

                // headers
                table
                    .append("thead")
                    .append("tr")
                    .selectAll("th")
                    .data(rows[0])
                    .enter()
                    .append("th")
                    .text(function (d) {
                    return d;
                    })
                    .style("border", "1px black solid")
                    .style("padding", "5px")
                    .style("background-color", "lightgray")
                    .style("font-weight", "bold")
                    .style("text-transform", "uppercase");

                // data
                table
                    .append("tbody")
                    .selectAll("tr")
                    .data(rows.slice(1))
                    .enter()
                    .append("tr")
                    .selectAll("td")
                    .data(function (d) {
                    return d;
                    })
                    .enter()
                    .append("td")
                    .style("border", "1px black solid")
                    .style("padding", "5px")
                    .text(function (d) {
                    return d;
                    })
                    .style("font-size", "12px");
                // console.log(rows)
                });

                """

    # print(javaScript)

    # path name
    save_path = "open-positions/report/js/"
    # file name
    file_name = f'{name}.js'
    # complete path
    complete_name = os.path.join(save_path, file_name)
    # write on the file
    output_file = open(complete_name, 'w')
    output_file.write(java_script)
    output_file.close()
