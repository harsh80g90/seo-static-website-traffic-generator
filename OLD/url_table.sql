CREATE TABLE IF NOT EXISTS url_data(
    id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    site_id int(11) DEFAULT NULL,
    offer_id int(11) DEFAULT NULL,
    organization varchar(255) DEFAULT NULL,
    url varchar(1000) DEFAULT NULL,
    zipcode varchar(255) DEFAULT NULL,
    city varchar(255) DEFAULT NULL,
    state varchar(255) DEFAULT NULL,
    date DATETIME DEFAULT NULL
);