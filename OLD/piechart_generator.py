"""File is responsible for generating javascript files and pie charts.

Files generated with this process will be saved in data folder.
"""

import os


def generate_javascript_pie(name):
    """
    Generate javascript graph from city name.

    Parameters:-
    name(string): city name.
    """
    javaScript = """
                // csv file we want to load
            let filename = "data/""" + name + """ +.csv";
            // console.log(filename.split('/')[1].split(".")[0])
            let title = "Number of jobs for a particular
                        Organization with respect to Education Level";

            // all of your code should be inside this command
            d3.csv(filename).then(function (loadedData) {
              // let's see if our data loaded correctly
              // (and take a peek at how it's formatted)

              // console.log(loadedData);

              // empty lists for our data and the labels
              let data = [];
              let labels = [];
              let color = [];

              // use a for-loop to load the data from the
              // file into our lists
              for (let i = 0; i < loadedData.length; i++) {
                // console.log(loadedData[i]);

                // get the year and mean temp for each listing
                // note: the 'keys' here correspond to the headers
                // in our file and need to be typed exactly
                let city_name = loadedData[i].level;
                let average_income = loadedData[i].level_count;

                // add the city to our labels
                labels.push(city_name);

                // and mean average_income to the data
                data.push(average_income);

                r = Math.floor(Math.random() * 255);
                g = Math.floor(Math.random() * 255);
                b = Math.floor(Math.random() * 255);

                c = "rgb(" + r + ", " + g + ", " + b + ")";

                color.push(c);
              }

              // basic line chart settings
              let options = {
                type: "doughnut",
                // type: "polarArea",
                data: {
                  labels: labels, // the labels we loaded!
                  datasets: [
                    {
                      data: data, // the data we loaded!
                      backgroundColor: color,
                      hoverOffset: 40,
                      label: "Population",
                    },
                  ],
                },
                options: {
                  title: {
                    display: true,
                    text: title,
                  },
                  legend: {
                    display: true,
                  },
                },
              };

              Chart.defaults.global.defaultFontSize = 15;

              // with all that done, we can create our chart!
              let chart = new Chart(document.getElementById("myChart"),
                                                            options);
            });

          // data table start
          d3.text(filename).then(function (data) {
            var rows = d3.csvParseRows(data),
            table = d3
              .select("#tableSection")
              .append("table")
              .style("border-collapse", "collapse")
              .style("border", "2px black solid");

            // headers
            table
              .append("thead")
              .append("tr")
              .selectAll("th")
              .data(rows[0])
              .enter()
              .append("th")
              .text(function (d) {
                return d;
              })
              .style("border", "1px black solid")
              .style("padding", "5px")
              .style("background-color", "lightgray")
              .style("font-weight", "bold")
              .style("text-transform", "uppercase");

            // data
            table
              .append("tbody")
              .selectAll("tr")
              .data(rows.slice(1))
              .enter()
              .append("tr")
              .selectAll("td")
              .data(function (d) {
                return d;
              })
              .enter()
              .append("td")
              .style("border", "1px black solid")
              .style("padding", "5px")
              .text(function (d) {
                return d;
              })
              .style("font-size", "12px");
            // console.log(rows)
          });

// data table end

               """

    # path name
    save_path = "open-positions/report/js//"
    # file name
    file_name = f'{name}.js'
    # complete path
    complete_name = os.path.join(save_path, file_name)
    # write on the file
    output_file = open(complete_name, 'w')
    output_file.write(javaScript)
    output_file.close()
