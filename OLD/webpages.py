"""File is responsible for fetching the data from MySql server.

and save it into data frame so that we can use it later to build our
data visualization system on web.
"""

import pandas as pd
import pdfkit
from html_generator import generate_html
from javascript_generator import generate_javascript
from piechart_generator import generate_javascript_pie
import os
import sys
from db import update_urls

# Header
HEADER = '''
--
# SEO Traffic Generator
## _See Data Visualization in real time with interactive graph_

## Features

- Some Feature 1
- Some Feature 1
- Some Feature 1
- Some Feature 1

'''

# Footer
FOOTER = '''
--
## Footer

Want to contribute? Great!

SEO Visualization for fast developing.
Make a change in your file and instantaneously see your updates!

Open your favorite Terminal and run these commands.
'''


def top_cities_by_population():
    """
    Get top cities in terms of population count
    """
    path = os.getenv("CSV_DATA")

    top_cities = pd.read_csv("" + path + "/cities.csv")
    top_cities = list(top_cities.loc[:, "city"])

    return top_cities


def add_under_score(test_string):
    """
    Sample function is used to add underscore in place of space.

    Parameters:-
    test_string(string): input string, example = 'grant park'.

    Returns:-
    new_string(string): processed output string, example = 'grant_park'.
    """
    if test_string.find(" ") == -1:
        new_string = test_string
    else:
        new_string = test_string.replace(" ", "-")

    return new_string


def get_html_reports(name):
    """
    Generate javascripts for each cities,
    generate url for each generate HTML file,
    store urls into database and csv file.

    Parameters:-
    name(string): Graph type, bar or pie.

    Returns:-
    url(string): generated urls for each HTML.
    """
    top_cities = top_cities_by_population()
    url = []

    for raw_city in top_cities:
        # now = datetime.now()
        # current_time = now.strftime("%m/%d/%Y %H:%M:%S")
        # heading = city
        # heading = '<h1> %s </h1>' % city
        # subheading = '<h3> Sample Report for %s </h3>' % city
        # header = '<div class="top">' + heading + subheading + '</div>'
        # footer = '<div class="bottom"> <h3> This Report has been Generated'+\
        #     current_time + '</h3> </div>'
        # html = header + footer
        # filename = city + ".html"
        # with open(filename, 'w') as file:
        #     file.write(html)

        # replace space with dash.
        city = add_under_score(raw_city)

        html_url = generate_html(HEADER, FOOTER, city)
        if name == "bar":
            generate_javascript(city)
        elif name == "pie":
            generate_javascript_pie(city)

        url.append(html_url)

        # update urls and city name into sql database.
        update_urls(html_url, raw_city)

    path = os.getenv("CSV_DATA")

    # city_data dataframe contains city names and urls
    city_data = pd.DataFrame({"city_name": top_cities,
                              "url": url})

    city_data.to_csv(''+path+'cities_url_data.csv',
                     index=False,
                     header=['city_name', 'url'])

    return url


def html_to_pdf():
    """
    Convert the html report to a pdf report.

    which can also be mailed along with the.
    html documents.
    """
    top_cities = top_cities_by_population()

    for city in top_cities:
        filename = city + ".html"
        filepdf = city + ".pdf"

        pdfkit.from_file(filename, filepdf)


if __name__ == "__main__":
    get_html_reports(sys.argv[1])
