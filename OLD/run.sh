#!/bin/bash

# activate python environment variables

#source ENV/bin/activate

source venv_seo/bin/activate

#source environment variables
source env.sh

#run lint
pylama .

#run python program
python pages_generator.py
