// this is making the bar chart
// for now i'm using sample data to check how it's work (later i'll change this with real dataset)

var chartColors = {
  red: "rgb(255, 99, 132)",
  blue: "rgb(54, 162, 235)",
};

var color = Chart.helpers.color;
var config = {
  type: "bar",
  data: {
    datasets: [
      {
        yAxisID: "average_income",
        // backgroundColor: color(chartColors.blue).alpha(1.2).rgbString(),
        // borderColor: "transparent",
        backgroundColor: [chartColors.blue, chartColors.red],
      },
    ],
  },
  plugins: [ChartDataSource],
  options: {
    title: {
      display: true,
      text: "Populationo and Average Income of a city",
      fontSize: 20,
    },
    scales: {
      xAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: "Population / Average Income",
          },
        },
      ],
      yAxes: [
        {
          id: "average_income",
          gridLines: {
            drawOnChartArea: false,
          },
          scaleLabel: {
            display: true,
            labelString: "Population (City)",
          },
        },
        {
          id: "city_name",
          position: "right",
          gridLines: {
            drawOnChartArea: false,
          },
          scaleLabel: {
            display: true,
            labelString: "Average Income",
          },
        },
      ],
    },
    plugins: {
      datasource: {
        type: "csv",
        url: "data/sample.csv", // for now i'm using simple data set to check how it works
        delimiter: ",",
        rowMapping: "dataset",
        datasetLabels: true,
        indexLabels: true,
      },
    },
  },
};

window.onload = function () {
  var ctx = document.getElementById("myChart").getContext("2d");
  window.myChart = new Chart(ctx, config);
};
