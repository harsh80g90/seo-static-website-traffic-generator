// csv file we want to load
let filename = "data/sample.csv";
let title = "Average Income In The City";

// all of your code should be inside this command
d3.csv(filename).then(function (loadedData) {
  // let's see if our data loaded correctly
  // (and take a peek at how it's formatted)

  // console.log(loadedData);

  // empty lists for our data and the labels
  let data = [];
  let labels = [];
  let color = [];

  // use a for-loop to load the data from the
  // file into our lists
  for (let i = 0; i < loadedData.length; i++) {
    // console.log(loadedData[i]);

    // get the year and mean temp for each listing
    // note: the 'keys' here correspond to the headers
    // in our file and need to be typed exactly
    let city_name = loadedData[i].city_name;
    let average_income = loadedData[i].average_income;

    // add the city to our labels
    labels.push(city_name);

    // and mean average_income to the data
    data.push(average_income);

    r = Math.floor(Math.random() * 255);
    g = Math.floor(Math.random() * 255);
    b = Math.floor(Math.random() * 255);

    c = "rgb(" + r + ", " + g + ", " + b + ")";

    color.push(c);
  }

  // basic line chart settings
  let options = {
    type: "doughnut",
    data: {
      labels: labels, // the labels we loaded!
      datasets: [
        {
          data: data, // the data we loaded!
          // borderColor: 'rgb(255,150,0)',
          backgroundColor: color,
          hoverOffset: 40,
          label: "Population",
        },
      ],
    },
    options: {
      title: {
        display: true,
        text: title,
      },
      legend: {
        display: true,
      },
    },
  };

  Chart.defaults.global.defaultFontSize = 15;

  // with all that done, we can create our chart!
  let chart = new Chart(document.getElementById("myChart"), options);
});

// pie chart

// all of your code should be inside this command
d3.csv(filename).then(function (loadedData) {
  // let's see if our data loaded correctly
  // (and take a peek at how it's formatted)

  // console.log(loadedData);

  // empty lists for our data and the labels
  let data = [];
  let labels = [];
  let color = [];

  // use a for-loop to load the data from the
  // file into our lists
  for (let i = 0; i < loadedData.length; i++) {
    // console.log(loadedData[i]);

    // get the year and mean temp for each listing
    // note: the 'keys' here correspond to the headers
    // in our file and need to be typed exactly
    let city_name = loadedData[i].city_name;
    let average_income = loadedData[i].population;

    // add the city to our labels
    labels.push(city_name);

    // and mean average_income to the data
    data.push(average_income);

    r = Math.floor(Math.random() * 255);
    g = Math.floor(Math.random() * 255);
    b = Math.floor(Math.random() * 255);

    c = "rgb(" + r + ", " + g + ", " + b + ")";

    color.push(c);
  }

  // basic line chart settings
  let options = {
    type: "pie",
    data: {
      labels: labels, // the labels we loaded!
      datasets: [
        {
          data: data, // the data we loaded!
          // borderColor: 'rgb(255,150,0)',
          backgroundColor: color,
          label: "Population",
          hoverOffset: 40,
        },
      ],
    },
    options: {
      title: {
        display: true,
        text: "Population in a City",
      },
      legend: {
        display: true,
      },
    },
  };

  Chart.defaults.global.defaultFontSize = 15;

  // with all that done, we can create our chart!
  let chart = new Chart(document.getElementById("pie"), options);
});
