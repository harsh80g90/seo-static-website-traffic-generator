# This file is responsible for generating static html file

from markdown import markdown
import os
from import_file import Static

# Header
HEADER = '''
--
#
## _See Data Visualization in real time with interactive graph_

## Features

- Some Feature 1
- Some Feature 1
- Some Feature 1
- Some Feature 1

'''

# Footer
FOOTER = '''
--
## Footer

Want to contribute? Great!

Make a change in your file and instantaneously see your updates!

Open your favorite Terminal and run these commands.
'''


#  generate_html generates the HTML file with the markdown
def generate_html(header, footer, filename):

    # read static data
    static_data = Static()

    # final html text
    final_html = ""

    # reading top_bar file
    top_bar = static_data.header()
    final_html += top_bar

    # conveting header and footer into html
    header_text = markdown(header)
    footer_text = markdown(footer)

    # adding header
    final_html += header_text

    # reading chart file
    chart_section = static_data.chart()
    final_html += chart_section

    # adding footer
    final_html += footer_text

    # reading bottom bar
    bottom_bar = static_data.footer(filename)
    final_html += bottom_bar

    # path name
    save_path = os.getenv("BASE_URL")
    # file name
    file_name = f'{filename}.html'
    # complete path
    complete_name = os.path.join(save_path, file_name)
    # write on the file
    output_file = open(complete_name, 'w')
    output_file.write(final_html)
    output_file.close()
    # close the file

    # for now i'm using VS code live server so for in this case
    domain = os.getenv("DOMAIN")
    
    url = f"{domain}/open-positions/report/{filename}.html"

    return url
