"""File is responsible for fetching raw data from MySql server,
and after preprocessing the raw data, data will be stored in csv format,
later this data will be used to generate charts.
"""

# importing the necessary libraires
import pandas as pd
from db import get_connection, get_data
import os


# Header
HEADER = '''
--
# SEO Traffic Generator
## _See Data Visualization in real time with interactive graph_

## Features

- Some Feature 1
- Some Feature 1
- Some Feature 1
- Some Feature 1

'''

# Footer
FOOTER = '''
--
## Footer

Want to contribute? Great!

SEO Visualization for fast developing.
Make a change in your file and instantaneously see your updates!

Open your favorite Terminal and run these commands.
'''


# Convert all periodicity salary into yearly salary
def get_yearly_salary_list(periodicity, salary):
    """
    Convert salary with different periodicity into yearly salary.

    Parameters:-
    periodicity(string): hourly, monthly, weekly...
    salary(int): salary figure

    Returns:-
    new_salary_list(int): salary list contains yearly salary.
    """
    salary_list = list(salary)
    new_salary_list = []

    for index, i in enumerate(periodicity):
        if i.lower() == 'yearly':
            # print("yearly_salary",salary_list[index])
            new_salary = 1 * int(salary_list[index])
            new_salary_list.append(new_salary)

        elif i.lower() == 'hourly':
            # print("hourly_salary",salary_list[index])
            new_salary = 2000 * salary_list[index]
            new_salary_list.append(new_salary)

        elif i.lower() == 'weekly':
            # print("weekly_salary",salary_list[index])
            new_salary = 50 * salary_list[index]
            new_salary_list.append(new_salary)

        elif i.lower() == 'monthly':
            # print("monthly_salary",salary_list[index])
            new_salary = 12 * salary_list[index]
            new_salary_list.append(new_salary)

        elif i.lower() == 'daily':
            # print("daily_salary",salary_list[index])
            new_salary = 200 * salary_list[index]
            new_salary_list.append(new_salary)

        else:
            new_salary = salary_list[index]
            new_salary_list.append(new_salary)

    return new_salary_list


# raw salary contains null records where salary present in range
# check_null_salary detect null salary values and
# replace null values to maximum salary.
def check_null_salary(salary, salary_in_range, max_salary):
    """
    Fill null salary values to maximum salary value in range.

    Parameters:-
    salary(int): salary figure.
    salary_in_range(bool): 1 if salary is in range, 0 if salary is not in range.
    max_salary(int): maximum salary figure in salary range.

    Returns:-
    new_salary(int): salary list contains yearly salary without null values.
    """
    new_salary = []
    for i in range(len(salary)):
        if salary[i] == -1 and salary_in_range[i] == 1:
            new_salary.append(max_salary[i])
        else:
            new_salary.append(salary[i])

    return new_salary


def generate_information():
    """
    Get graph data from database using sql query,
    store each city data into separate csv file and generate HTML file.
    """
    # database connection
    db = get_connection()

    query = """select job.salary_exact as salary,
            job.salary_periodicity as salary_periodicity,
            job.salary_range_start as min_salary,
            job.salary_range_end as max_salary,
            job.salary_is_range as salary_in_range,
            predicted_job_category.name as job_category,
            lower(location.city_name) as city,
            location.zipcode as zipcode,
            location.state as state
            from job
            join job2location on job.id = job2location.job_id
            join location on job2location.location_id = location.id
            join predicted_job_category on
            job.predicted_category_id = predicted_job_category.id
            where salary_extract = 1 and location.city_name != ''
            limit 50000"""

    # query to get the raw data
    raw_data = get_data(db, query)

    # get information from raw data for graph plotting
    project_data = pd.DataFrame(raw_data, columns=['salary',
                                                   'salary_periodicity',
                                                   'min_salary',
                                                   'max_salary',
                                                   'salary_in_range',
                                                   'job_category',
                                                   'city',
                                                   'zipcode',
                                                   'state'])

    # preprocessing raw data for graph
    project_data['original_salary'] = check_null_salary(
                                      list(project_data['salary']),
                                      list(project_data['salary_in_range']),
                                      list(project_data['max_salary']))

    project_data['original_salary'] = get_yearly_salary_list(
                                      project_data['salary_periodicity'],
                                      project_data['original_salary'])

    sample_data = project_data[['job_category', 'city', 'original_salary']]

    path = os.getenv("DATA_PATH")

    city_data = set(sample_data['city'])

    city_dataframe = pd.DataFrame(city_data, columns=['city'])
    city_dataframe.to_csv(''+path+'cities.csv', index=False, header=['city'])

    for city in city_data:

        # select data for each city
        sample_city_data = sample_data.loc[sample_data['city'] == city]

        # sort rows by salary data
        sorted_salary_data = sample_city_data.sort_values(
            'original_salary', ascending=False)

        # select top 10 unique professions
        final_data = sorted_salary_data.drop_duplicates(
            subset=['job_category', 'city'], keep='last')[:10]

        final_data.to_csv('' + path +'/'+ city + '.csv',
                          index=False,
                          header=['job_category',
                                  'city',
                                  'salary'])

    print("*Complete fetching information*")

    # Html Generator
    # generate_html(HEADER, FOOTER, output_file)


# function to get data for education level piechart
def generate_donut_chart_info():
    """
    Get graph data from database using sql query,
    store each city data into separate csv file and generate HTML file.

    """
    # database connection
    db = get_connection()

    query = """select education_level_id,
                count(education_level_id),
                organization
                from job
                where html_job_description_hash in
                    (select
                    distinct html_job_description_hash
                    from job
                    where education_level_id != 0)
                group by organization, education_level_id"""

    # query to get the raw data
    raw_data = get_data(db, query)

    # get information from raw data for graph plotting
    project_data = pd.DataFrame(raw_data, columns=['education_level_id',
                                                   'education_level_count',
                                                   'organization'])

    path = os.getenv("DATA_PATH")

    # get organization list to create seprate csv files
    organization_list = set(project_data['organization'])

    organization_dataframe = pd.DataFrame(organization_list,
                                          columns=['organization'])

    organization_dataframe.to_csv(''+path+'organizations.csv',
                                  index=False,
                                  header=['organization'])

    for organization in organization_list:

        organization_data = project_data.loc[
            project_data['organization'] == organization]

        final_data = organization_data[['education_level', 'education_level_count']]

        final_data.to_csv('' + path + organization + '.csv',
                          index=False,
                          header=['education_level',
                                  'education_level_count'])

    print("*Complete fetching information*")

    # Html Generator
    # generate_html(HEADER, FOOTER, output_file)


if __name__ == "__main__":
    generate_information()
