import os
from datetime import datetime
from jinja2 import Environment, PackageLoader
from markdown2 import markdown

import pandas as pd 

from jsGenerator import generate_javascript

POSTS = {}

for markdown_post in os.listdir('content'):
    file_path = os.path.join('content', markdown_post)

    with open(file_path, 'r') as file:
        POSTS[markdown_post] = markdown(file.read(), extras=['metadata'])

    POSTS = {
        post: POSTS[post] for post in sorted(POSTS, key=lambda post: datetime.strptime(POSTS[post].metadata['date'], '%Y-%m-%d'), reverse=True)
    }


env = Environment(loader=PackageLoader('main', 'templates'))
home_template = env.get_template('home.html')
post_template = env.get_template('post.html')

for post in POSTS:
    html_table = ""
    post_metadata = POSTS[post].metadata

    post_data = {
        'content': POSTS[post],
        'title': post_metadata['title'],
        'date': post_metadata['date'],
    }

    csv_data = pd.read_csv(f"open-positions/report/data/{post_metadata['slug']}.csv")
    html_table += csv_data.to_html(index=False)

    u = {
        "js_name": post_metadata['slug'],
        "html_table":html_table
    }
    post_html = post_template.render(post=post_data, urls=u)

    post_file_path = 'open-positions/report/{slug}.html'.format(
        slug=post_metadata['slug'])

    os.makedirs(os.path.dirname(post_file_path), exist_ok=True)
    # make html
    with open(post_file_path, 'w') as file:
        file.write(post_html)

    # make js
    generate_javascript(post_metadata['slug'])

posts_metadata = [POSTS[post].metadata for post in POSTS]
tags = [post['tags'] for post in posts_metadata]
home_html = home_template.render(posts=posts_metadata, tags=tags)

with open('open-positions/report/home.html', 'w') as file:
    file.write(home_html)
