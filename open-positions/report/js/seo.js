$(document).ready(function () {
  const NextIcon =
    '<img class=" img-fluid btn1 next-slide" width="40" src="./images/right_icon.svg" alt="right">';
  const PrevIcon =
    '<img class=" img-fluid btn1 prev-slide" width="40" src="./images/left_icon.svg" alt="left">';
  $(".owl-carousel").owlCarousel({
    loop: true,
    margin: 25,
    nav: true,
    navText: [PrevIcon, NextIcon],
    // navText:["<button class='btn1 prev-slide'> <img class='w-75 img-fluid' src='./images/left_icon.svg' alt='left'>  </button>","<button class='btn1 next-slide'> <img class='w-75 img-fluid' src='./images/right_icon.svg' alt='right'>  </button>"],
    dots: false,
    responsive: {
      0: {
        items: 1,
        nav: true,
      },
      600: {
        items: 2,
        nav: true,
      },
      1000: {
        items: 4,
        nav: true,
      },
    },
  });

  $(".owl-carousel").find(".owl-nav").removeClass("disabled");
  $(".owl-carousel").on("changed.owl.carousel", function (event) {
    $(this).find(".owl-nav").removeClass("disabled");
  });
});
