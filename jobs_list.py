"""File is responsible for getting job list for each city,
into json format.
"""


from db import get_connection, get_data
import json
import os


def get_job_list(city_name, location_id):
    """
    Get list of jobs from database using sql query.

    return:-
    job_list: job list into json format.
    """
    # database connection
    db = get_connection()

    query = """select job_title
                from job
                join job2location on job.id = job2location.job_id
                join location on job2location.location_id = location.id
                where job.id > 1000 and is_expired = 0
                and location.id = {} order by job.last_crawl_date desc"""

    # query to get the raw data
    job_list = get_data(db, query.format(location_id))

    data = []
    for job in job_list:

        json_format = {"jobs": ""+job[0]+""}
        data.append(json_format)

    path = os.getenv('JSON_DATA')

    with open(f"{path}/{city_name}.json", "w") as outfile:
        json.dump(data, outfile)
