# refrence  :- https://www.rankranger.com/schema-markup-generator

def generate_jsonld(url, heading, org_name, pub_name):

    schema = """
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "Article",
    "mainEntityOfPage": {
      "@type": "WebPage",
      "@id": "%s"
    },
    "headline": "%s",
    "image": "",  
    "author": {
      "@type": "Organization",
      "name": "%s"
    },  
    "publisher": {
      "@type": "Organization",
      "name": "%s",
      "logo": {
        "@type": "ImageObject",
        "url": ""
      }
    },
    "datePublished": ""
  }
  </script>
  """ % (url, heading, org_name, pub_name)

    print(schema)


generate_jsonld("https://www.google.com", "This is google page",
                "Google.com", "Himanshu Tripathi")
