---
title: London
date: 2019-04-17
tags: three
thumbnail: images/new_york.png
summary: Landon population of 8,336,817 distributed over about 302.6 square miles (784 km2), 
slug: landon
---


__About__


Situated on one of the world's largest natural harbors, New York City is composed of five boroughs, each of which is a county of the State of New York. The five boroughs—Brooklyn, Queens, Manhattan, the Bronx, and Staten Island—were created when local governments were consolidated into a single city in 1898.[20] The city and its metropolitan area constitute the premier gateway for legal immigration to the United States. As many as 800 languages are spoken in New York,[21] making it the most linguistically diverse city in the world. New York is home to more than 3.2 million residents born outside the United States,[22] the largest foreign-born population of any city in the world as of 2016.[23][24] As of 2019, the New York metropolitan area is estimated to produce a gross metropolitan product (GMP) of $2.0 trillion. If the New York metropolitan area were a sovereign state, it would have the eighth-largest economy in the world. New York is home to the highest number of billionaires of any city in the world.[25]