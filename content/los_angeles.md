---
title: Los Angeles
date: 2019-04-10
tags: Los Angeles
thumbnail: images/huston.png
summary: Nainital about
slug: los-angeles
---

__About__

Nainital is a Himalayan resort town in the Kumaon region of India’s Uttarakhand state, at an elevation of roughly 2,000m. Formerly a British hill station, it’s set around Nainital Lake, a popular boating site with Naina Devi Hindu Temple on its north shore. A cable car runs to Snow View observation point (at 2,270m), with vistas over the town and mountains including Nanda Devi, Uttarakhand’s highest peak
