---
title: United States
date: 2019-04-10
tags: four
thumbnail: images/chicago.png
summary: United States (US), often simply called New York, is the most populous city in the United States. With an estimated 2019 population of 8,336,817 distributed over
slug: united-states
---

__About__

New York City (NYC), often simply called New York, is the most populous city in the United States. With an estimated 2019 population of 8,336,817 distributed over about 302.6 square miles (784 km2), New York City is also the most densely populated major city in the United States.[11] Located at the southern tip of the State of New York, the city is the center of the New York metropolitan area, the largest metropolitan area in the world by urban landmass.[12] With almost 20 million people in its metropolitan statistical area and approximately 23 million in its combined statistical area, it is one of the world's most populous megacities. New York City has been described as the cultural, financial, and media capital of the world, significantly influencing commerce,[13] entertainment, research, technology, education, politics, tourism, art, fashion, and sports, and is the most photographed city in the world.[14] Home to the headquarters of the United Nations,[15] New York is an important center for international diplomacy,[16][17] and has sometimes been called the capital of the world.[18][19]
